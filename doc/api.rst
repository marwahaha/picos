.. _api:

API Reference
=============

PICOS is organized in a number of submodules and subpackages, most of which you
do not need to access directly when solving optimization problems. It is usually
sufficient to ``import picos`` and use the functions and classes provided in the
:mod:`picos` namespace. A notable exception are the tools for handling uncertain
data that are found in the :mod:`picos.uncertain` namespace.

.. rubric:: Modules

.. toctree::
  :includehidden:
  :maxdepth: 2

  api/picos
