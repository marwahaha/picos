.. _userguide:

Usage Notes
===========

.. toctree::
   :maxdepth: 2

   numscipy.rst
   slicing.rst
   duals.rst
   tolerances.rst
   cheatsheet.rst
